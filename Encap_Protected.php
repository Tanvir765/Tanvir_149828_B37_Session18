<?php
class MyClass{
    protected $name="Tanvir Khan";
    function display(){
        echo $this->name ;
    }
    function get(){
        $this->display();
    }
}
class MySub extends MyClass
{
    public function SubClass()
    {
        $obj = new MyClass();
        $obj->get();
    }
}

$obj1=new MySub();
$obj1->SubClass();
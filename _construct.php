<?php
class Animal{

    public function __construct() {
        $this->created = time();
    }

}

$tux = new Animal();
echo $tux->created;
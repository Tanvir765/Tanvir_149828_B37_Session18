<?php
class Customer {
    private $name; private $credit_card_number;
    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
    public function setCC($cc) {
        $this->credit_card_number = $cc;
    }
    public function getCC() {
        return $this->credit_card_number;
    }
    public function __sleep() {
        return array("name");
    }
    public function __wakeup() {
        if($this->name == "Sunil") {
            //you would ideally fetch CC data from Database
            $this->credit_card_number = "1234567890123456";
        }
    }
}
$c = new Customer();
$c->setName("Sunil");
$c->setCC("1234567890123456");
$data = serialize($c)."\n";
var_dump(unserialize($data));